/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import ejb.BookService;
import entities.Book;
import entities.BookCategory;
import entities.Customer;
import entities.Loan;
import helper.DateHelper;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

/**
 *
 * @author Alex
 */
@Named(value = "loanController")
@SessionScoped
public class LoanController implements Serializable {

    @EJB
    BookService bookService;
    private Loan loan;
    private Customer cus;
    private Book b;
    private Boolean loanButton;
    
    public String createLoan(Book book) {
        loan = new Loan();
        String userName = getCurrentUserName();
        for(Customer c: bookService.getAllUsers()){
            if(c.getName().equals(userName)){
                cus = c;
            }
        }
        b = book;

        loan.setUser(cus);
        loan.setBook(b);
        loan.setLoanDate(DateHelper.getCurrentDate());
        b.getLoan().add(loan);
        cus.getLoan().add(loan);
        bookService.addLoan(loan);
        b.setAvailability(Boolean.FALSE);
        
        bookService.updateBook(b);
        bookService.updateUser(cus);
        return "index";
        
    }
    public String returnBook(Loan loan) {
        loan.getBook().setAvailability(true);
        bookService.updateBook(loan.getBook());
        bookService.deleteLoan(loan);
        return "manageLoan";
        
    }
    public String getCurrentUserName() {
        FacesContext facesContext = FacesContext.getCurrentInstance();       
        return facesContext.getExternalContext().getRemoteUser();
    }
    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public Book getB() {
        return b;
    }

    public void setB(Book b) {
        this.b = b;
    }
    
    public Customer getCus() {
        
        return cus;
    }
    
    public void setCus(Customer cus) {
        this.cus = cus;
    }

    public Boolean getLoanButton(Book b) {
        if(b.getAvailability()){
            loanButton = false;
        }
        else{
            loanButton = true;
        }
        return loanButton;
    }

    public void setLoanButton(Boolean loanButton) {
        this.loanButton = loanButton;
    }
    
    public List<Loan> getUserLoan() {
        List<Loan> loans = new ArrayList<Loan>();
        for (Loan l : bookService.getAllLoans()){
            if (l.getUser().getName().equals(getCurrentUserName())) {
                loans.add(l);
            }
        }
        return loans;
            
    }
    
}
