/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import ejb.BookService;
import entities.BookCategory;
import entities.Comment;
import entities.Customer;
import helper.DateHelper;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

/**
 *
 * @author Alex
 */
@Named(value = "commentController")
@SessionScoped
public class CommentController implements Serializable {

   @EJB
    BookService bookService;
    private Comment comment;
    private String commentMsg;
    private Customer cus;
    private BookCategory bc;

    public String createComment() {
        comment = new Comment();
        comment.setComment(commentMsg);
        comment.setCommentTime(DateHelper.getCurrentDate());
        for(Customer c: bookService.getAllUsers()){
            if(c.getName().equals(getCurrentUserName())){
                cus = c;
            }
        }
        comment.setCus(cus);
        comment.setBookCategory(bc);
        bookService.addComment(comment);
        bc.getComments().add(comment);
        cus.getComments().add(comment);
        bookService.updateBookCategory(bc);
        bookService.updateUser(cus);
        return "bookDetail";
    }
    public Comment getComment() {
        return comment;
    }
    
    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public Customer getCus() {
        return cus;
    }

    public void setCus(Customer cus) {
        this.cus = cus;
    }
    public String getCurrentUserName() {
        FacesContext facesContext = FacesContext.getCurrentInstance();       
        return facesContext.getExternalContext().getRemoteUser();
    }
    public String getCommentMsg() {
        return commentMsg;
    }

    public void setCommentMsg(String commentMsg) {
        this.commentMsg = commentMsg;
    }
    public String readBookCategory(BookCategory bg) {
        this.bc = bg;
        return "bookDetail";
    }
    public List<Comment> getUserComment() {
        return bc.getComments();
    }

    public BookCategory getBc() {
        return bc;
    }

    public void setBc(BookCategory bc) {
        this.bc = bc;
    }
    
}
