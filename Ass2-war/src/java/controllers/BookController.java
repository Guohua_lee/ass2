/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import ejb.BookService;
import entities.*;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

/**
 *
 * @author Alex
 */
@Named(value = "bookController")
@SessionScoped
public class BookController implements Serializable {

    @EJB
    BookService bookService;
    private BookCategory bookCategory;
    private int quantity;
    private Customer cus;
    private List<BookCategory> bookCategories;
    private String search;
    private Boolean searchFlag = false;

    @PostConstruct
    public void init() {
        String userName = getCurrentUserName();
        for (Customer c : bookService.getAllUsers()) {
            if (c.getName().equals(userName)) {
                cus = c;
            }
        }
        

    }

    public BookCategory getBookCategory() {
        return bookCategory;
    }

    public String setupCreate() {
        this.bookCategory = new BookCategory();
        quantity = 0;
        return "bookManagement";
    }

    public void setBookCategory(BookCategory bookCategory) {
        this.bookCategory = bookCategory;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String read(BookCategory bookCategory) {
        this.bookCategory = bookCategory;
        this.quantity = bookCategory.getBooks().size();
        return "bookManagement";
    }
    
    public String detail(BookCategory bookCategory) {
        this.bookCategory = bookCategory;
        return "bookDetail";
    }

    public String create() {
        bookCategory.setImgSource("img/book.png");
        bookService.addBookCategory(bookCategory);
        Long isbn = bookCategory.getIsbn();
        BookCategory bc = bookService.findBookCategoryByISBN(isbn);
        List<Book> bookList = new ArrayList<Book>();
        for (int i = 0; i < quantity; i++) {
            Book book = new Book();
            book.setAvailability(Boolean.TRUE);
            book.setBookCategory(bc);
            bookService.addBook(book);
            bookList.add(book);
            bc.setBooks(bookList);
        }
        bookService.updateBookCategory(bc);
        return "index";
    }

    public String markBook(BookCategory bc) {

        if (bc.getCustomers().contains(cus)) {
            bc.getCustomers().remove(cus);
            cus.getBookmarks().remove(bc);

        } else {
            cus.getBookmarks().add(bc);
            bc.getCustomers().add(cus);
        }
        bookService.updateUser(cus);
        bookService.updateBookCategory(bc);
        return "index";
    }

    public String markStatus(BookCategory bc) {
        String marker = "mark";
        if (bc.getCustomers().contains(cus)) {
            marker = "unmark";
        }
        return marker;
    }

    public String update() {

        bookService.deleteCategoryBooks(bookCategory.getId());
        List<Book> bookList = bookCategory.getBooks();
        bookList.clear();
        bookCategory.setBooks(bookList);
        for (int i = 0; i < quantity; i++) {
            Book book = new Book();
            book.setAvailability(Boolean.TRUE);
            book.setBookCategory(bookCategory);
            bookService.addBook(book);
            bookCategory.getBooks().add(book);
        }
        bookService.updateBookCategory(bookCategory);
        return "index";
    }

    public String getCurrentUserName() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return facesContext.getExternalContext().getRemoteUser();
    }

    public String delete(BookCategory bookCategory) {

        try {
            bookService.deleteCategoryBooks(bookCategory.getId());
            bookService.deleteBookCategory(bookCategory);
            return "index";
        } catch (Exception e) {
            ErrorController ec = new ErrorController();
            ec.setErrorMessage("The book can not be deleted");
            return "errorPage";
        }
    }

    public List<BookCategory> getBookCategories() {
        return bookCategories;
    }

    public String resetBookCategories() {
        if(!searchFlag){
            this.bookCategories = bookService.getAllBookCategorys();
        }
        searchFlag = false;
        return "index";
    }

    public int bookQuantity(BookCategory bookCategory) {
        List<Book> books = bookCategory.getBooks();
        return books.size();
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public void searchBook() {
        searchFlag = true;
        bookCategories.clear();
        for (BookCategory b : bookService.getAllBookCategorys()) {
            if (b.getAuthor().equals(search)) {
                bookCategories.add(b);
            } else if (b.getTitle().equals(search)) {
                bookCategories.add(b);
            } else if (b.getPublisher().equals(search)) {
                bookCategories.add(b);
            }
        }
    }
}
