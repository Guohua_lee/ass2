/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author Alex
 */
@Named(value = "errorController")
@SessionScoped
public class ErrorController implements Serializable {

    /**
     * Creates a new instance of ErrorController
     */
    private static String errorMessage;
    private String error;

    public String getError() {
        error = errorMessage;
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
    
    public static String getErrorMessage() {
        return errorMessage;
    }

    public static void setErrorMessage(String errorMessage) {
        ErrorController.errorMessage = errorMessage;
    }
    
}
