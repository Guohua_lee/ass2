/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import ejb.BookService;
import entities.Customer;
import entities.UserGroup;
import helper.PasswordConverter;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

/**
 *
 * @author Alex
 */
@Named(value = "customerController")
@SessionScoped
public class CustomerController implements Serializable {

    /**
     * Creates a new instance of CustomerController
     */
    @EJB
    BookService bookService;
    private Customer customer = new Customer();
    String password;
    String groupName;
    ArrayList<String> group = new ArrayList<String>();    
    @PostConstruct
    public void init() {
        group.add("admin");
        group.add("customer");
      
    }
    
    public String read(Customer customer) {
        this.customer = customer;
        return "customerManagement";
    }
    public String setupCreate() {
        customer = new Customer();
        return "customerManagement";
    }
    public Boolean checkCustomerName() {
        Boolean flag = true;
        for(Customer c: bookService.getAllUsers()){
            if(c.getName().equals(customer.getName())){
                flag = false;
            }
        }
        return flag;
        
    }
    public String create() {
        if(checkCustomerName()){
        customer.setPassword(PasswordConverter.convertString(password));
        bookService.addUser(customer);
        Customer cus = bookService.findByUserName(customer.getName());
        UserGroup userGroup = new UserGroup();
        userGroup.setGroupName(groupName);
        userGroup.setName(cus.getName());
        userGroup.setCus(cus);
        bookService.addUserGroup(userGroup);
        cus.setUserGroup(userGroup);
        bookService.updateUser(cus);
        return "index";
        }
        else {
            return null;
        }
    }
    
    
    public String update() {
        if(checkCustomerName()){
        bookService.deleteGroupUser(customer.getId());
        UserGroup userGroup = new UserGroup();
        userGroup.setGroupName(groupName);
        userGroup.setName(customer.getName());
        userGroup.setCus(customer);
        bookService.addUserGroup(userGroup);
        customer.setUserGroup(userGroup);
        customer.setPassword(PasswordConverter.convertString(password));
        bookService.updateUser(customer);   
        return "index";
        }
        else{
            return null;
        }
    }
    
    public String delete(Customer c) {
        try{
        bookService.deleteGroupUser(customer.getId());
        bookService.deleteUser(c);
        }
        catch (Exception e){
            ErrorController ec= new ErrorController();
            ec.setErrorMessage("The user can not be deleted");
            return "errorPage";         
        }
        return "index";
    }
    
    public List<Customer> getCustomers() {
        return bookService.getAllUsers();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;      
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public ArrayList<String> getGroup() {
        return group;
    }

    public void setGroup(ArrayList<String> group) {
        this.group = group;
    }
    
}
