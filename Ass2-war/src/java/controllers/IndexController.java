/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import ejb.BookService;
import entities.*;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Alex
 */
@Named(value = "indexController")
@ApplicationScoped
public class IndexController {
    
    
    @EJB
    BookService bookService;
    private String currentUsername;

        
    
    public String getCurrentUsername() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        currentUsername = facesContext.getExternalContext().getRemoteUser();
        return currentUsername;
    }

    public void setCurrentUsername(String currentUsername) {
        this.currentUsername = currentUsername;
    }
    
    
    public List<Customer> getUsers() {
        return bookService.getAllUsers();
    }
    
    public List<BookCategory> getBookCategories() {
        return bookService.getAllBookCategorys();
    }
    public List<BookCategory> getMarkedBook() {
        List<BookCategory> bcs = new ArrayList<BookCategory>();
        for(BookCategory bc: getBookCategories()){
            if(bc.getCustomers().size()>0){
                bcs.add(bc);
            }
        }
        return bcs;
    }
    public List<Book> getBooks() {
        return bookService.getAllBooks();
    }
   
    public String logout() {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    HttpSession httpSession = (HttpSession)facesContext.getExternalContext().getSession(false);
    httpSession.invalidate();
    
    return "/faces/index";
    }
}
