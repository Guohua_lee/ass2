package entities;

import entities.BookCategory;
import entities.Comment;
import entities.Loan;
import entities.UserGroup;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-10-26T19:20:18")
@StaticMetamodel(Customer.class)
public class Customer_ { 

    public static volatile SingularAttribute<Customer, Long> id;
    public static volatile ListAttribute<Customer, BookCategory> bookmarks;
    public static volatile SingularAttribute<Customer, String> name;
    public static volatile SingularAttribute<Customer, UserGroup> userGroup;
    public static volatile SingularAttribute<Customer, String> password;
    public static volatile ListAttribute<Customer, Loan> loan;
    public static volatile ListAttribute<Customer, Comment> comments;

}