package entities;

import entities.Book;
import entities.Customer;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-10-26T19:20:18")
@StaticMetamodel(Loan.class)
public class Loan_ { 

    public static volatile SingularAttribute<Loan, Long> id;
    public static volatile SingularAttribute<Loan, Customer> cus;
    public static volatile SingularAttribute<Loan, Book> book;
    public static volatile SingularAttribute<Loan, Date> loanDate;

}