package entities;

import entities.BookCategory;
import entities.Loan;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-10-26T19:20:18")
@StaticMetamodel(Book.class)
public class Book_ { 

    public static volatile SingularAttribute<Book, Long> id;
    public static volatile SingularAttribute<Book, BookCategory> bookCategory;
    public static volatile ListAttribute<Book, Loan> loan;
    public static volatile SingularAttribute<Book, Boolean> availability;

}