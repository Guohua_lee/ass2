package entities;

import entities.Book;
import entities.Comment;
import entities.Customer;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-10-26T19:20:18")
@StaticMetamodel(BookCategory.class)
public class BookCategory_ { 

    public static volatile SingularAttribute<BookCategory, Long> id;
    public static volatile SingularAttribute<BookCategory, String> author;
    public static volatile ListAttribute<BookCategory, Book> books;
    public static volatile SingularAttribute<BookCategory, String> title;
    public static volatile SingularAttribute<BookCategory, String> description;
    public static volatile SingularAttribute<BookCategory, Long> isbn;
    public static volatile SingularAttribute<BookCategory, String> imgSource;
    public static volatile ListAttribute<BookCategory, Customer> customers;
    public static volatile SingularAttribute<BookCategory, String> publishDate;
    public static volatile ListAttribute<BookCategory, Comment> comments;
    public static volatile SingularAttribute<BookCategory, String> publisher;

}