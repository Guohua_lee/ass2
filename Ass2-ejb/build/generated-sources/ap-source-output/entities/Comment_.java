package entities;

import entities.BookCategory;
import entities.Customer;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-10-26T19:20:18")
@StaticMetamodel(Comment.class)
public class Comment_ { 

    public static volatile SingularAttribute<Comment, Long> id;
    public static volatile SingularAttribute<Comment, Date> commentTime;
    public static volatile SingularAttribute<Comment, BookCategory> bookCategory;
    public static volatile SingularAttribute<Comment, Customer> cus;
    public static volatile SingularAttribute<Comment, String> comment;

}