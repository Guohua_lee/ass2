/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.*;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author Alex
 */
@Stateless
@LocalBean
public class BookService {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @PersistenceContext(unitName = "Ass2-ejbPU")
    EntityManager em;

    /*
     * Customer CRUD
     */
    public void addUser(Customer c) {
        em.persist(c);
    }

    public void updateUser(Customer c) {
        em.merge(c);
    }

    public void deleteUser(Customer c) {
        em.remove(em.merge(c));
    }

    public Customer findUser(Long id) {
        return em.find(Customer.class, id);
    }
    public Customer findByUserName(String name) {
        Customer c = null;
        try
        {
            c = (Customer) em.createNamedQuery("Customer.findByName")
                    .setParameter("name", name)
                    .getSingleResult();
        }
        catch(Exception e){
            
        }
        return c;
    }
    public List<Customer> getAllUsers() {
        TypedQuery<Customer> q = em.createQuery("SELECT c FROM Customer c", Customer.class);
        return q.getResultList();
    }

    /*
     * BookCategory CRUD
     */
    public void addBookCategory(BookCategory bc) {
        em.persist(bc);
    }

    public void updateBookCategory(BookCategory bc) {
        em.merge(bc);
    }

    public void deleteBookCategory(BookCategory bc) {
        em.remove(em.merge(bc));
    }

    public BookCategory findBookCategory(Long id) {
        return em.find(BookCategory.class, id);
    }

    public BookCategory findBookCategoryByISBN(Long isbn) {
        BookCategory bc = null;

        try {
            bc = (BookCategory) em.createNamedQuery("BookCategory.findByIsbn")
                    .setParameter("isbn", isbn)
                    .getSingleResult();
        } catch (Exception ex) {
            return null;
        }

        return bc;
    }

    public List<BookCategory> getAllBookCategorys() {
        TypedQuery<BookCategory> q = em.createQuery("SELECT b FROM BookCategory b", BookCategory.class);
        return q.getResultList();
    }

    /*
     * Book CRUD
     */
    public void addBook(Book b) {
        em.persist(b);
    }

    public void updateBook(Book b) {
        em.merge(b);
    }

    public void deleteBook(Book b) {
        em.remove(em.merge(b));
    }

    public void deleteCategoryBooks(Long id) {
        Query q = em.createNativeQuery("DELETE FROM Book b WHERE b.BookCategory =?");
        q.setParameter(1, id);
        q.executeUpdate();

    }

    public Book findBook(Long id) {
        return em.find(Book.class, id);
    }
    
    public List<Book> getAllBooks() {
        TypedQuery<Book> q = em.createQuery("SELECT b FROM Book b", Book.class);
        return q.getResultList();
    }

    /*
     * Loan CRUD
     */
    public void addLoan(Loan l) {
        em.persist(l);
    }

    public void updateLoan(Loan l) {
        em.merge(l);
    }

    public void deleteLoan(Loan l) {
        em.remove(em.merge(l));
    }

    public Loan findLoan(Long id) {
        return em.find(Loan.class, id);
    }

    public List<Loan> getAllLoans() {
        TypedQuery<Loan> q = em.createQuery("SELECT l FROM Loan l", Loan.class);
        return q.getResultList();
    }

    /*
     * Comment CRUD
     */
    public void addComment(Comment c) {
        em.persist(c);
    }

    public void updateComment(Comment c) {
        em.merge(c);
    }

    public void deleteComment(Comment c) {
        em.remove(em.merge(c));
    }

    public Comment findComment(Long id) {
        return em.find(Comment.class, id);
    }

    public List<Comment> getAllComments() {
        TypedQuery<Comment> q = em.createQuery("SELECT c FROM Comment c", Comment.class);
        return q.getResultList();
    }
    /*
     * UserGroup CRUD
     */

    public void addUserGroup(UserGroup u) {
        em.persist(u);
    }

    public void updateUserGroup(UserGroup u) {
        em.merge(u);
    }

    public void deleteUserGroup(UserGroup u) {
        em.remove(em.merge(u));
    }
    public void deleteGroupUser(Long id) {
        Query q = em.createNativeQuery("DELETE FROM UserGroup ug WHERE ug.cus =?");
        q.setParameter(1, id);
        q.executeUpdate();
    }
    public UserGroup findUserGroup(Long id) {
        return em.find(UserGroup.class, id);
    }

    public UserGroup findUserGroupByUserId(Long id) {
        UserGroup ug = null;
        try {
            ug = (UserGroup) em.createNamedQuery("UserGroup.findByCustomerId")
                    .setParameter("cusId", id)
                    .getSingleResult();
        } catch (Exception ex) {
            return null;
        }
        return ug;
    }

    public List<UserGroup> getAllUserGroups() {
        TypedQuery<UserGroup> q = em.createQuery("SELECT u FROM Usergroup u", UserGroup.class);
        return q.getResultList();
    }
}
